package com.koflox.slideshowapp;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import modules.SlidePagerAdapter;

public class MainActivity extends AppCompatActivity {

    public static final String PREF_KEY_SOURCE_ROUTE = "pref_key_source_route";
    public static final String PREF_KEY_INTERVAL = "pref_key_interval";
    public static final String PREF_KEY_AUTO_SHOW = "pref_key_auto_show";
    public static final String PREF_KEY_START_TIME = "pref_key_start_time";
    public static final String PREF_KEY_END_TIME = "pref_key_end_time";
    public static final String PREF_KEY_REBOOT_AUTO_LAUNCH = "pref_key_reboot_auto_launch";
    public static final String PREF_KEY_CHARGE_AUTO_LAUNCH = "pref_key_charge_auto_launch";

    public static final String KEY_CUR_SLIDE = "cur_slide";
    public static final String KEY_SHOW_SLIDE = "showing_slides";
    public static final String KEY_IMAGE_LIST = "image_list";

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_LOCATION = 101;

    private SharedPreferences settings;
    private PowerReceiver powerReceiver = new PowerReceiver();
    private ArrayList<String> imageList = new ArrayList<>();
    private File chosenFolder;
    private Timer slideTimer;
    private boolean showingSlides;
    private int currentSlide, curInterval;
    private Handler handler;

    private SlidePagerAdapter pagerAdapter;
    private ViewPager slidePager;
    private Button startShowButton, stopShowButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startShowButton = (Button) findViewById(R.id.startShowButton);
        stopShowButton = (Button) findViewById(R.id.stopShowButton);

        setButtonsEnabledState();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        settings = PreferenceManager.getDefaultSharedPreferences(this);
        String prefRoute = settings.getString(PREF_KEY_SOURCE_ROUTE, "does not exist");
        if ((prefRoute.equals("")) || (prefRoute == null)) {
            File root = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
            SharedPreferences.Editor editor = settings.edit();
            String rootPath = root.getPath();
            editor.putString(PREF_KEY_SOURCE_ROUTE, rootPath);
            editor.apply();
        }

        if (!settings.contains(PREF_KEY_INTERVAL)) {
            settings.edit().putInt(PREF_KEY_INTERVAL, 2).commit();
        }

        slidePager = (ViewPager) findViewById(R.id.slidePager);

        if (getIntent().getBooleanExtra("start_show_key", false)) {
            startShowButton.performClick();
/*
            handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            stopShowButton.performClick();
                        }
                    });
                }
            }, 5000);
            */
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void getImages() {
        if (settings.contains(PREF_KEY_SOURCE_ROUTE)) {
            chosenFolder = new File(settings.getString(PREF_KEY_SOURCE_ROUTE, "does not exist"));
            File[] fileList = chosenFolder.listFiles();
            for (File file : fileList) {
                if (file.getName().endsWith(".png")
                        || file.getName().endsWith(".jpg")
                        || file.getName().endsWith(".jpeg")
                        || file.getName().endsWith(".bmp")) {
                    imageList.add(file.getPath());
                }
            }
        }
    }

    public void onSettingsClick(MenuItem item) {
        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
        startActivity(intent);
    }

    public void onStartShowButtonClick(View view) {
        if ((android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M)
                && (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_LOCATION);
        } else {
            currentSlide = 0;
            displayImages();
        }
    }

    public void onStopShowButtonClick(View view) {
        showingSlides = false;
        currentSlide = 0;
        slideTimer.cancel();
        slideTimer.purge();
        setButtonsEnabledState();
        Toast.makeText(this, "Slide show stopped", Toast.LENGTH_SHORT).show();
    }

    public void displayImages() {
        imageList.clear();
        getImages();
        if (!imageList.isEmpty()) {
            showingSlides = true;
            setButtonsEnabledState();

            curInterval = settings.getInt(PREF_KEY_INTERVAL, 2);

            pagerAdapter = new SlidePagerAdapter(MainActivity.this, imageList);
            slidePager.setAdapter(pagerAdapter);
            slidePager.setCurrentItem(0, false);

            slideTimer = new Timer();
            slideTimer.scheduleAtFixedRate(new SlideTask(), 0, curInterval * 1000);
        } else {
            Toast.makeText(this, "Choose a directory containing pictures. \n You can do it in settings.", Toast.LENGTH_SHORT).show();
        }
    }

    class SlideTask extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (currentSlide > imageList.size() - 1) {
                        currentSlide = 0;
                    } else {
                        slidePager.setCurrentItem(currentSlide++, false);
                    }
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    currentSlide = 0;
                    displayImages();
                } else {
                    Toast.makeText(this, "Read external storage permission is denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    private void setButtonsEnabledState() {
        if (showingSlides) {
            startShowButton.setEnabled(false);
            stopShowButton.setEnabled(true);
        } else {
            startShowButton.setEnabled(true);
            stopShowButton.setEnabled(false);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(KEY_CUR_SLIDE, slidePager.getCurrentItem());
        outState.putBoolean(KEY_SHOW_SLIDE, showingSlides);
        outState.putStringArrayList(KEY_IMAGE_LIST, imageList);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {
            currentSlide = savedInstanceState.getInt(KEY_CUR_SLIDE);
            showingSlides = savedInstanceState.getBoolean(KEY_SHOW_SLIDE);

            imageList = savedInstanceState.getStringArrayList(KEY_IMAGE_LIST);
            if (showingSlides) {
                slidePager.setAdapter(pagerAdapter);
            } else {
                pagerAdapter = new SlidePagerAdapter(MainActivity.this, imageList);
                slidePager.setAdapter(pagerAdapter);
                slidePager.setCurrentItem(currentSlide);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (settings.getBoolean(PREF_KEY_CHARGE_AUTO_LAUNCH, false)) {
            this.registerReceiver(powerReceiver, new IntentFilter("android.intent.action.ACTION_POWER_CONNECTED"));
        }
        if (showingSlides) {
            displayImages();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (settings.getBoolean(PREF_KEY_CHARGE_AUTO_LAUNCH, false)) {
            this.unregisterReceiver(powerReceiver);
        }
        if (slidePager != null) {
            currentSlide = slidePager.getCurrentItem();
        }
        if (showingSlides) {
            slideTimer.cancel();
            slideTimer.purge();
        }
    }

    public class PowerReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (startShowButton.isEnabled()) {
                startShowButton.performClick();
            }
        }
    }
}
