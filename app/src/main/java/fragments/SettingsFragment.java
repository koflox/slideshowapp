package fragments;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.TimePicker;
import android.widget.Toast;

import com.koflox.slideshowapp.R;

import java.util.Calendar;

import modules.StartShowReceiver;

public class SettingsFragment extends PreferenceFragment
                             implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String PREF_KEY_SOURCE_ROUTE = "pref_key_source_route";
    public static final String PREF_KEY_INTERVAL = "pref_key_interval";
    public static final String PREF_KEY_AUTO_SHOW = "pref_key_auto_show";
    public static final String PREF_KEY_START_TIME = "pref_key_start_time";
    public static final String PREF_KEY_END_TIME = "pref_key_end_time";
    public static final String PREF_KEY_REBOOT_AUTO_LAUNCH = "pref_key_reboot_auto_launch";
    public static final String PREF_KEY_CHARGE_AUTO_LAUNCH = "pref_key_charge_auto_launch";

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_LOCATION = 101;

    SharedPreferences settings;
    AlarmManager am;
    PendingIntent pendingIntent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        addPreferencesFromResource(R.xml.preferences);

        Preference prefImageSource = findPreference("pref_key_source_route");
        prefImageSource.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if ((android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M)
                        && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED)){
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_LOCATION);
                } else {
                    showFileDialog();
                }
                return true;
            }
        });

        Preference prefInteval = findPreference("pref_key_interval");
        prefInteval.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                FragmentTransaction frTransaction = getFragmentManager().beginTransaction();
                Fragment prevIntervalPickerDialog = getFragmentManager().findFragmentByTag("intervalPickerDialog");
                if (prevIntervalPickerDialog != null) {
                    frTransaction.remove(prevIntervalPickerDialog);
                }

                IntervalPickerFragment newIntervalPickerDialog = new IntervalPickerFragment();
                newIntervalPickerDialog.show(frTransaction, "intervalPickerDialog");
                return true;
            }
        });

        Preference prefStartTime = findPreference("pref_key_start_time");
        prefStartTime.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                int prefHour;
                int prefMin;
                long prefTimeInMillis = settings.getLong(PREF_KEY_START_TIME, -1);
                if (prefTimeInMillis != (-1)) {
                    prefHour = (int) prefTimeInMillis / 1000 / 3600;
                    prefMin = (int) (prefTimeInMillis / 1000) % 3600 / 60;
                } else {
                    prefHour = 12;
                    prefMin = 0;
                }
                TimePickerDialog timepickerdialog = new TimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                settings.edit().putLong(PREF_KEY_START_TIME, hourOfDay * 60 * 60 * 1000 + minute * 60 * 1000).apply();

                                Toast.makeText(getActivity(), "Время запуска: " + hourOfDay + ":" + minute, Toast.LENGTH_SHORT).show();
                            }
                        }, prefHour, prefMin, true);
                timepickerdialog.show();
                return true;
            }
        });

        Preference prefEndTime = findPreference("pref_key_end_time");
        prefEndTime.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                TimePickerDialog timepickerdialog = new TimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                settings.edit().putLong(PREF_KEY_END_TIME, hourOfDay * 60 * 60 * 1000 + minute * 60 * 1000).apply();

                                Toast.makeText(getActivity(), "Время остановки: " + hourOfDay + ":" + minute, Toast.LENGTH_SHORT).show();
                            }
                        }, 12, 00, true);
                timepickerdialog.show();
                return true;
            }
        });
    }

    public void showFileDialog() {
        FragmentTransaction frTransaction = getFragmentManager().beginTransaction();
        Fragment prevFileDialog = getFragmentManager().findFragmentByTag("fileDialog");
        if (prevFileDialog != null) {
            frTransaction.remove(prevFileDialog);
        }

        FileDialogFragment newFileDialog = new FileDialogFragment();
        newFileDialog.show(frTransaction, "fileDialog");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    showFileDialog();
                } else {
                    Toast.makeText(getActivity(), "Read external storage permission is denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case PREF_KEY_AUTO_SHOW:
                startAutoLaunch();
                break;
            case PREF_KEY_START_TIME:
                startAutoLaunch();
                break;
        }
    }

    public void startAutoLaunch() {
        if (settings.getBoolean(PREF_KEY_AUTO_SHOW, false)) {
            if (am != null) {
                am.cancel(pendingIntent);
                pendingIntent.cancel();
            }
            am = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(getActivity(), StartShowReceiver.class);
            pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, PendingIntent.FLAG_ONE_SHOT);

            Calendar currentDate = Calendar.getInstance();
            long currentTime = currentDate.getTimeInMillis();
            long startAppTimeInMillis = settings.getLong(PREF_KEY_START_TIME, -1);
            if (startAppTimeInMillis != (-1)) {
                Calendar futureDate = Calendar.getInstance();
                int startHour = (int) startAppTimeInMillis / 1000 / 3600;
                int startMin = (int) (startAppTimeInMillis / 1000) % 3600 / 60 ;
                futureDate.set(Calendar.HOUR_OF_DAY, startHour);
                futureDate.set(Calendar.MINUTE, startMin);
                long futureTime = futureDate.getTimeInMillis();
                if (futureTime < currentTime) {
                    futureDate.add(Calendar.DAY_OF_MONTH, 1);
                    futureTime = futureDate.getTimeInMillis();
                }

                long differenceInTime = futureTime - currentTime;
                am.set(AlarmManager.RTC_WAKEUP, currentTime + differenceInTime, pendingIntent);
            }
        } else {
            if (am != null) {
                am.cancel(pendingIntent);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }
}
