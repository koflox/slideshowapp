package fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.koflox.slideshowapp.R;

public class IntervalPickerFragment extends DialogFragment {

    public static final String PREF_KEY_INTERVAL = "pref_key_interval";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final SharedPreferences settings;
        settings = PreferenceManager.getDefaultSharedPreferences(getActivity());

        final Dialog intervalPickerDialog = new Dialog(getActivity());
        intervalPickerDialog.setTitle("Choose interval");
        intervalPickerDialog.setContentView(R.layout.interval_dialog_layout);
        intervalPickerDialog.setCancelable(true);
        intervalPickerDialog.setCanceledOnTouchOutside(true);
        final NumberPicker numberPicker = (NumberPicker) intervalPickerDialog.findViewById(R.id.interval_picker);
        numberPicker.setMaxValue(60);
        numberPicker.setMinValue(1);
        numberPicker.setWrapSelectorWheel(false);

        if (settings.contains(PREF_KEY_INTERVAL)) {
            int curInterval =  settings.getInt(PREF_KEY_INTERVAL, 1);
            numberPicker.setValue(curInterval);
        }
        numberPicker.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt(PREF_KEY_INTERVAL, numberPicker.getValue());
                editor.apply();

                intervalPickerDialog.dismiss();
                Toast.makeText(getActivity(), "Chosen interval is " + String.valueOf(numberPicker.getValue()) + " s", Toast.LENGTH_SHORT).show();
            }
        });

        return intervalPickerDialog;
    }
}
