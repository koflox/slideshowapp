package modules;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.koflox.slideshowapp.SplashActivity;

public class BootReceiver extends BroadcastReceiver{

    public static final String PREF_KEY_REBOOT_AUTO_LAUNCH = "pref_key_reboot_auto_launch";

    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences setting = PreferenceManager.getDefaultSharedPreferences(context);
        if (setting.getBoolean(PREF_KEY_REBOOT_AUTO_LAUNCH, false)) {
            if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
                Intent i = new Intent(context, SplashActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        }
    }
}
