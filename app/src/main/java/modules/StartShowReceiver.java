package modules;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.koflox.slideshowapp.MainActivity;
import com.koflox.slideshowapp.SplashActivity;

public class StartShowReceiver extends BroadcastReceiver {

    public static final String WHERE_MY_CAT_ACTION = "com.koflox.action.CAT";
    public static final String ALARM_MESSAGE = "Срочно пришлите кота!";

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("start_show_key", true);
        context.startActivity(i);
    }
}
